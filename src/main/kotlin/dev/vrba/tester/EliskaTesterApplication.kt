package dev.vrba.tester

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.context.properties.ConfigurationPropertiesScan
import org.springframework.boot.runApplication

@SpringBootApplication
@ConfigurationPropertiesScan(basePackages = ["dev.vrba.tester.configuration"])
class EliskaTesterApplication

fun main(args: Array<String>) {
    runApplication<EliskaTesterApplication>(*args)
}
