package dev.vrba.tester.controllers

import dev.vrba.tester.dtos.*
import dev.vrba.tester.entities.QuestionsCollection
import dev.vrba.tester.exceptions.QuestionsCollectionNotFoundException
import dev.vrba.tester.services.QuestionsService
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.multipart.MultipartFile
import java.util.UUID

@RestController
@RequestMapping("/api/v1/collections")
class QuestionsCollectionsController(private val questionsService: QuestionsService) {

    @GetMapping
    fun listQuestionsCollections(): ResponseEntity<Iterable<QuestionsCollectionDto>> {
        val collections = questionsService.listQuestionsCollections()
        val dtos = collections.map(QuestionsCollection::toDto)

        return ResponseEntity.ok(dtos)
    }

    @GetMapping("/{id}")
    fun findQuestionsCollection(@PathVariable("id") id: UUID): ResponseEntity<QuestionsCollectionDetailDto> {
        val collection = questionsService.findQuestionsCollection(id) ?: throw QuestionsCollectionNotFoundException()
        val dto = collection.toDetailDto()

        return ResponseEntity.ok(dto)
    }

    data class CreateQuestionsCollectionRequest(val name: String)

    @PostMapping
    fun createQuestionsCollection(@RequestBody request: CreateQuestionsCollectionRequest): ResponseEntity<QuestionsCollectionDto> {
        val collection = questionsService.createQuestionsCollection(request.name)
        val dto = collection.toDto()

        return ResponseEntity.ok(dto)
    }

    @PostMapping("/{id}/create-question", consumes = [MediaType.MULTIPART_FORM_DATA_VALUE])
    fun createQuestion(
        @PathVariable("id") id: UUID,
        @RequestParam("image") file: MultipartFile,
        @RequestParam("answer") answer: String
    ) : ResponseEntity<QuestionDto> {
        val question = questionsService.addQuestionToCollection(id, answer, file)
        val dto = question.toDto()

        return ResponseEntity.ok(dto)
    }

    @DeleteMapping("/{collection}")
    fun deleteQuestionsCollection(@PathVariable("collection") collection: UUID): ResponseEntity<*> {
        questionsService.deleteQuestionsCollection(collection)

        return ResponseEntity.ok(mapOf("message" to "Questions collection deleted"))
    }

    @DeleteMapping("/question/{question}")
    fun deleteQuestion(@PathVariable("question") question: UUID): ResponseEntity<*> {
        questionsService.deleteQuestion(question)

        return ResponseEntity.ok(mapOf("message" to "Question deleted"))
    }
}