package dev.vrba.tester.controllers

import dev.vrba.tester.dtos.QuestionDto
import dev.vrba.tester.dtos.toDto
import dev.vrba.tester.services.QuestionsService
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import java.util.*

@RestController
@RequestMapping("/api/v1/questions")
class QuestionsController(private val service: QuestionsService) {

    @GetMapping("/{collection}/next-question")
    fun nextQuestion(@PathVariable("collection") collection: UUID): ResponseEntity<QuestionDto> {
        val question = service.getNextQuestion(collection)
        val dto = question.toDto()

        return ResponseEntity.ok(dto)
    }

    data class AnswerQuestionRequest(val correct: Boolean)

    @PostMapping("/{collection}/{question}/answer")
    fun answerQuestion(
        @PathVariable("collection") collectionId: UUID,
        @PathVariable("question") questionId: UUID,
        @RequestBody request: AnswerQuestionRequest
    ): ResponseEntity<QuestionDto> {
        // Store the question progress...
        service.answerQuestion(questionId, request.correct)

        val question = service.getNextQuestion(collectionId, questionId)
        val dto = question.toDto()

        return ResponseEntity.ok(dto)
    }

    @PostMapping("/{question}/reset")
    fun resetQuestionProgress(@PathVariable("question") questionId: UUID): ResponseEntity<QuestionDto> {
        val question = service.resetQuestion(questionId)
        val dto = question.toDto()

        return ResponseEntity.ok(dto)
    }
}