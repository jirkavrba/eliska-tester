package dev.vrba.tester.dtos

import dev.vrba.tester.entities.Question
import java.time.LocalDateTime
import java.util.UUID

data class QuestionDto(
    val id: UUID,
    val image: String,
    val answer: String,
    val lastViewedAt: LocalDateTime,
    val viewsCount: Int,
    val successesCount: Int
)

fun Question.toDto(): QuestionDto = QuestionDto(
    this.id,
    this.image,
    this.answer,
    this.lastViewedAt,
    this.viewsCount,
    this.successesCount
)