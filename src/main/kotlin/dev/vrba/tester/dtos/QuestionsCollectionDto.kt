package dev.vrba.tester.dtos

import dev.vrba.tester.entities.Question
import dev.vrba.tester.entities.QuestionsCollection
import java.util.UUID

data class QuestionsCollectionDto(val id: UUID, val name: String, val questions: Int, val image: String?)

data class QuestionsCollectionDetailDto(
    val id: UUID,
    val name: String,
    val questions: List<QuestionDto>,
)

fun QuestionsCollection.toDto(): QuestionsCollectionDto = QuestionsCollectionDto(
    this.id,
    this.name,
    this.questions.size,
    this.questions.firstOrNull()?.image
)

fun QuestionsCollection.toDetailDto(): QuestionsCollectionDetailDto = QuestionsCollectionDetailDto(
    this.id,
    this.name,
    this.questions.map(Question::toDto)
)