package dev.vrba.tester.entities

import java.time.LocalDateTime
import java.util.*
import javax.persistence.*

@Entity
@Table(name = "questions")
class Question(
    @Id
    val id: UUID = UUID.randomUUID(),

    @Column(nullable = false)
    val image: String,

    @Column(nullable = false)
    val answer: String,

    @Column(name = "last_viewed_at", nullable = false)
    var lastViewedAt: LocalDateTime = LocalDateTime.now(),

    @Column(name = "views_count", nullable = false)
    var viewsCount: Int = 0,

    @Column(name = "successes_count", nullable = false)
    var successesCount: Int = 0,

    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    @JoinColumn(name = "collection_id", nullable = false)
    var collection: QuestionsCollection
)