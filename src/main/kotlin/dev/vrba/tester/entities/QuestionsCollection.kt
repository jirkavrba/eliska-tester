package dev.vrba.tester.entities

import org.hibernate.annotations.NotFound
import org.hibernate.annotations.NotFoundAction
import java.util.UUID
import javax.persistence.*

@Entity
@Table(name = "questions_collection")
class QuestionsCollection(
    @Id
    val id: UUID = UUID.randomUUID(),

    @Column(nullable = false)
    val name: String,

    @NotFound(action = NotFoundAction.IGNORE)
    @OneToMany(fetch = FetchType.EAGER, mappedBy = "collection", cascade = [CascadeType.REMOVE])
    val questions: MutableList<Question> = mutableListOf()
)