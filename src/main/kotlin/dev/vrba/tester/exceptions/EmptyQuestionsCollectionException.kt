package dev.vrba.tester.exceptions

import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ResponseStatus

@ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
class EmptyQuestionsCollectionException : RuntimeException("The questions collection does not contains any elements")