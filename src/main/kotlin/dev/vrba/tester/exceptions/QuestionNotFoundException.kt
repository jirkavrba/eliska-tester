package dev.vrba.tester.exceptions

import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ResponseStatus

@ResponseStatus(HttpStatus.NOT_FOUND)
class QuestionNotFoundException : RuntimeException("Question collection not found.")