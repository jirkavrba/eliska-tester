package dev.vrba.tester.repositories

import dev.vrba.tester.entities.QuestionsCollection
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository
import java.util.UUID

@Repository
interface QuestionsCollectionsRepository : CrudRepository<QuestionsCollection, UUID>