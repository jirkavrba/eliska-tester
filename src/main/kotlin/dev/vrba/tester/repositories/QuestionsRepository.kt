package dev.vrba.tester.repositories

import dev.vrba.tester.entities.Question
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository
import java.util.UUID

@Repository
interface QuestionsRepository : CrudRepository<Question, UUID>