package dev.vrba.tester.services

import com.amazonaws.services.s3.AmazonS3
import com.amazonaws.services.s3.model.ObjectMetadata
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service
import org.springframework.web.multipart.MultipartFile
import java.net.URL

@Service
class ImageUploadingService(
    @Value("\${aws.bucket-name}")
    private val bucket: String,
    private val s3: AmazonS3
) {
    fun upload(name: String, file: MultipartFile): URL {
        val stream = file.inputStream
        val meta = ObjectMetadata()

        meta.contentLength = file.size
        meta.contentType = file.contentType

        s3.putObject(bucket, name, stream, meta)

        return s3.getUrl(bucket, name)
    }

    fun delete(name: String) {
        s3.deleteObject(bucket, name)
    }
}