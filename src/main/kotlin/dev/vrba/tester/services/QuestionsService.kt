package dev.vrba.tester.services

import dev.vrba.tester.entities.Question
import dev.vrba.tester.entities.QuestionsCollection
import dev.vrba.tester.exceptions.EmptyQuestionsCollectionException
import dev.vrba.tester.exceptions.QuestionNotFoundException
import dev.vrba.tester.exceptions.QuestionsCollectionNotFoundException
import dev.vrba.tester.repositories.QuestionsCollectionsRepository
import dev.vrba.tester.repositories.QuestionsRepository
import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Service
import org.springframework.web.multipart.MultipartFile
import java.time.Duration
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.Period
import java.util.*
import kotlin.math.max
import kotlin.math.round

@Service
class QuestionsService(
    private val imagesService: ImageUploadingService,
    private val questionsRepository: QuestionsRepository,
    private val collectionsRepository: QuestionsCollectionsRepository
) {
    fun listQuestionsCollections(): Iterable<QuestionsCollection> =
        collectionsRepository.findAll()

    fun findQuestionsCollection(id: UUID): QuestionsCollection? =
        collectionsRepository.findByIdOrNull(id)

    fun createQuestionsCollection(name: String): QuestionsCollection =
        collectionsRepository.save(QuestionsCollection(name = name))

    fun addQuestionToCollection(collectionId: UUID, answer: String, file: MultipartFile): Question {
        val filename = UUID.randomUUID().toString()
        val image = imagesService.upload(filename, file).toString()

        val collection = findQuestionsCollection(collectionId) ?: throw QuestionsCollectionNotFoundException()
        val question = Question(image = image, answer = answer, collection = collection)

        questionsRepository.save(question)

        return question
    }

    fun deleteQuestion(questionId: UUID) {
        val question = questionsRepository.findByIdOrNull(questionId) ?: throw QuestionNotFoundException()
        val image = question.image.split("/").last()

        imagesService.delete(image)
        questionsRepository.delete(question)
    }

    fun deleteQuestionsCollection(collectionId: UUID) {
        val collection = collectionsRepository.findByIdOrNull(collectionId) ?: throw QuestionsCollectionNotFoundException()

        // Delete all images associated with questions from this collection from the AWS S3 bucket
        collection.questions
            .map { it.image.split("/").last() }
            .forEach(imagesService::delete)

        collectionsRepository.delete(collection)
    }

    fun getNextQuestion(collectionId: UUID, lastQuestionId: UUID? = null): Question {
        val collection = collectionsRepository.findByIdOrNull(collectionId) ?: throw QuestionsCollectionNotFoundException()
        return collection.questions
            .filter { it.id != lastQuestionId }
            .maxByOrNull {
                val diff = Duration.between(it.lastViewedAt, LocalDateTime.now())
                val delay = round(diff.toSeconds() / 10f)
                val accuracy = it.successesCount / it.viewsCount.toFloat()
                val score = delay * (1f / accuracy) * (1f / it.viewsCount)

                score
            }
            ?: throw EmptyQuestionsCollectionException()
    }

    fun answerQuestion(questionId: UUID, correct: Boolean) {
        val question = questionsRepository.findByIdOrNull(questionId) ?: throw QuestionNotFoundException()

        question.viewsCount++
        question.successesCount += if (correct) 1 else 0
        question.lastViewedAt = LocalDateTime.now()

        questionsRepository.save(question)
    }

    fun resetQuestion(questionId: UUID): Question {
        val question = questionsRepository.findByIdOrNull(questionId) ?: throw QuestionNotFoundException()

        question.viewsCount = 0
        question.successesCount = 0
        question.lastViewedAt = LocalDateTime.now()

        return questionsRepository.save(question)
    }

}