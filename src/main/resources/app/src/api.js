const root = "/api/v1"

export const fetchQuestionsCollections = async () => {
    return await fetch(root + "/collections")
        .then(response => response.json())
        .then(response => response);
}

export const fetchQuestionsCollection = async (id) => {
    return await fetch(root + `/collections/${id}`)
        .then(response => response.ok ? response.json() : null);
}

export const fetchNextQuestion = async (id) => {
    return await fetch(root + `/questions/${id}/next-question`)
        .then(response => response.ok ? response.json() : null);
}

export const answerQuestion = async (collection, question, correct) => {
    return await fetch(root + `/questions/${collection}/${question}/answer`, {
        method: "POST",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify({ correct })
    })
    .then(response => response.ok ? response.json() : null);
}

export const createQuestionsCollection = async (name) => {
    await fetch(root + "/collections", {
        method: "POST",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify({
            name
        })
    });
}

export const createQuestion = async (collection, answer, image) => {
    const data =new FormData();

    data.append("answer", answer);
    data.append("image", image, "image");

    await fetch(root + `/collections/${collection}/create-question`, {
        method: "POST",
        body: data
    });
}

export const resetQuestionProgress = async (id) => {
    await fetch(root + `/questions/${id}/reset`, { method: "POST" });
}

export const deleteQuestion = async (id) => {
    await fetch(root + `/collections/question/${id}`, { method: "DELETE" });
}