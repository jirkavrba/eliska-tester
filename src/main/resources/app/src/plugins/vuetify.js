// Styles
import '@mdi/font/css/materialdesignicons.css'
import 'vuetify/styles'

// Vuetify
import { createVuetify } from 'vuetify'

// https://vuetifyjs.com/en/introduction/why-vuetify/#feature-guides

const custom = {
    colors: {
        primary: "#4CAF50"
    }
}

export default createVuetify({
    theme: {
        defaultTheme: "custom",
        themes: {
            custom
        }
    }
})
