import { createRouter, createWebHashHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'
import CollectionView from "../views/CollectionView.vue";
import TestingView from "../views/TestingView.vue";

const router = createRouter({
  history: createWebHashHistory(),
  routes: [
    {
      path: '/',
      name: 'home',
      component: HomeView
    },
    {
      path: '/collection/:id',
      name: 'collection',
      component: CollectionView
    },
    {
      path: '/test/:id',
      name: 'testing',
      component: TestingView
    }
  ]
})

export default router
